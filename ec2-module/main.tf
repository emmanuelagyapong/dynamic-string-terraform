resource "aws_instance" "default-ec2" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name      = var.key_name
  user_data     = file("${path.module}/script.sh")
  subnet_id     = aws_subnet.subnet1.id
  security_groups = [aws_security_group.allow_tls.id]
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}


resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound and outbound rules for traffic"
  vpc_id      = aws_vpc.main.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
}


resource "aws_subnet" "subnet1" {
  vpc_id     = resource.aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = resource.aws_vpc.main.id
}

resource "aws_route_table" "RouteTable" {
  vpc_id = resource.aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gateway.id
  }
}

resource "aws_route_table_association" "public_subnet_asso" {
  route_table_id = aws_route_table.RouteTable.id
  subnet_id      = aws_subnet.subnet1.id
}